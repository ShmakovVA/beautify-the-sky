from os import path

from flask import Flask
from flask import g
from flask_pymongo import PyMongo
from flask_restful import Api

from bts.core import SkyBeautifier
from rest_api import settings
from rest_api.db import DB, SQLite_DB
from rest_api.file_backend import FileBackend

app = Flask(__name__)
app.config.from_object(settings)
mongo = PyMongo(app=app, uri=settings.MONGO_URI, connecttimeoutms=5000)
api = Api(app)
file_backend = FileBackend()
db = DB()
bts = SkyBeautifier()


@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()


if settings.DISABLE_MONGO:
    if not path.exists(settings.SQLITE3_DB):
        SQLite_DB.init_db()

import rest_api.resources
