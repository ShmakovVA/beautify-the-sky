import os

from flask import send_from_directory

from rest_api.settings import (
    UPLOAD_DIRECTORY,
    RESULT_DIRECTORY,
    SEGMENTS_DIRECTORY
)


class FileBackend():
    def __init__(self):
        self._prepare_directories()

    @staticmethod
    def _prepare_directories():
        for dir in [UPLOAD_DIRECTORY, RESULT_DIRECTORY, SEGMENTS_DIRECTORY]:
            if not os.path.exists(dir):
                os.makedirs(dir)

    @staticmethod
    def _build_path(filename, storage):
        return os.path.join(storage, filename)

    def is_exists(self, filename, storage):
        return os.path.exists(self._build_path(filename, storage))

    def store(self, filename, file_bytes, storage, mode='wb'):
        file_path = self._build_path(filename, storage)
        with open(file_path, mode) as f:
            f.write(file_bytes)
        return file_path

    @staticmethod
    def get_file_as_response(filename, storage):
        return send_from_directory(storage, filename, as_attachment=True)
