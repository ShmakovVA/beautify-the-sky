import os

ROOT_PATH = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

DISABLE_MONGO = True
MONGO_URI = os.environ.get('MONGO_URL', 'mongodb://localhost:27017/bts')
SQLITE3_DB = os.path.join(ROOT_PATH, 'database.db')

UPLOAD_DIRECTORY = os.path.join(ROOT_PATH, 'files_storage')
SEGMENTS_DIRECTORY = os.path.join(ROOT_PATH, 'files_segmented')
RESULT_DIRECTORY = os.path.join(ROOT_PATH, 'files_storage_processed')

MAX_CONTENT_LENGTH = 16 * 1024 * 1024
MAX_FILE_SIZE = MAX_CONTENT_LENGTH

CLEAR_SKY_PATH = f'{ROOT_PATH}/bts/clear-sky.jpg'
PTH_MODEL_PATH = f'{ROOT_PATH}/bts/best_model.pth'

GOOGLE_DRIVE_DOWNLOAD_URL = "https://docs.google.com/uc?export=download"
GOOGLE_DRIVE_PTH_ID = "1qvByn3gfE12DjZaHDwlgW-fDVJNffVIJ"
PTH_URL = (f"{GOOGLE_DRIVE_DOWNLOAD_URL}&id={GOOGLE_DRIVE_PTH_ID}")
