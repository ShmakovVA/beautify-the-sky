import logging
from subprocess import Popen, PIPE

from rest_api.settings import (
    GOOGLE_DRIVE_PTH_ID,
    GOOGLE_DRIVE_DOWNLOAD_URL,
    PTH_URL
)

logger = logging.getLogger(__name__)


def catch_file_in_request(key, request):
    file_bytes = filename = None
    _files = request.files
    if _files and key in _files:
        _file = _files[key]
        filename = _file.filename
        try:
            file_bytes = _file.read()
        except Exception as e:
            logger.error(f'File {filename} reading error: {e}')
    return file_bytes, filename


def fetch_model(model_path):
    cmd_1 = Popen(["wget", "--quiet", "--save-cookies",
                   "/tmp/cookies.txt", "--keep-session-cookies",
                   "--no-check-certificate", PTH_URL, "-O-"],
                  stdout=PIPE)
    confirm = str(cmd_1.communicate()[0]).split('confirm=')[-1].split(
        '&', 1)[0]
    link = (f"{GOOGLE_DRIVE_DOWNLOAD_URL}&confirm={confirm}"
            f"&id={GOOGLE_DRIVE_PTH_ID}")
    cmd_2 = Popen(["wget", "--load-cookies", "/tmp/cookies.txt", link,
                   "-O", model_path],
                  stdout=PIPE)
    cmd_2.wait()
