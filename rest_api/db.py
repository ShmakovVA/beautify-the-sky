import logging
import sqlite3

from flask import g

from rest_api.settings import SQLITE3_DB, DISABLE_MONGO

logger = logging.getLogger(__name__)


class SQLite_DB(object):

    @classmethod
    def _get_db(cls):
        db = getattr(g, '_database', None)
        if db is None:
            db = g._database = sqlite3.connect(SQLITE3_DB)
            db.row_factory = sqlite3.Row
        return db

    @classmethod
    def script(cls, script):
        from rest_api import app
        with app.app_context():
            db = cls._get_db()
            db.cursor().executescript(script)
            db.commit()

    @classmethod
    def init_db(cls):
        script = 'create table file(name varchar(50), path varchar(255), reason varchar(20));'
        cls.script(script)

    @classmethod
    def insert_file(cls, payload):
        name, path, reason = [payload[key] for key in
                              ['name', 'path', 'reason']]
        script = f'insert into "file" values ("{name}", "{path}", "{reason}");'
        cls.script(script)


class DB():
    def add_file(self, filename, file_path, reason):
        from rest_api import mongo
        payload = {'name': filename,
                   'path': file_path,
                   'reason': reason}
        if DISABLE_MONGO:
            logger.warning('MongoDB is disabled in settings ! '
                           'sqlite3 will be used...')
            logger.warning(payload)
            SQLite_DB.insert_file(payload=payload)
        else:
            logger.warning(f'Writing to db: {payload}')
            try:
                id = mongo.db.file.insert(payload)
            except Exception as e:
                logger.warning(f'MongoDB is not reachable ! {e}')
