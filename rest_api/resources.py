import logging

from flask import jsonify, request, make_response
from flask_restful import Resource

from rest_api import api, file_backend, db, app, bts
from rest_api.settings import (
    UPLOAD_DIRECTORY,
    RESULT_DIRECTORY
)
from rest_api.utils import catch_file_in_request

logger = logging.getLogger(__name__)


@app.errorhandler(404)
def page_not_found(e):
    return make_response(
        jsonify({'error': 'Ooops... there are nothing for you. Sorry.'}), 404)


class Root(Resource):
    def get(self):
        message = {'message': 'Service is online'}
        return make_response(jsonify(message), 200)


class File(Resource):
    def get(self):
        filename = request.args.get('filename', None)
        is_file_exists = file_backend.is_exists(filename=filename,
                                                storage=UPLOAD_DIRECTORY)
        if filename and is_file_exists:
            resp = make_response(
                file_backend.get_file_as_response(filename=filename,
                                                  storage=UPLOAD_DIRECTORY),
                200)
        else:
            resp = make_response(jsonify({'error': 'File does not exist!'}),
                                 404)
        return resp

    def post(self):
        file_bytes, filename = catch_file_in_request(key='file',
                                                     request=request)
        if file_bytes and filename:
            file_path = file_backend.store(filename=filename,
                                           file_bytes=file_bytes,
                                           storage=UPLOAD_DIRECTORY,
                                           mode='wb')
            added_to_db = db.add_file(filename=filename, file_path=file_path,
                                      reason='just upload')
            resp = make_response(
                jsonify({'message': 'File added successfully!'}), 201)
        else:
            resp = make_response(
                jsonify({'error': 'You should send an image file!'}),
                404)
        return resp


class Processing(Resource):
    def post(self):
        file_bytes, filename = catch_file_in_request(key='file',
                                                     request=request)
        if file_bytes and filename:
            file_path = file_backend.store(filename=filename,
                                           file_bytes=file_bytes,
                                           storage=UPLOAD_DIRECTORY,
                                           mode='wb')
            added_to_db = db.add_file(filename=filename, file_path=file_path,
                                      reason='full processing')
            filename = self._do_action(source=f'{UPLOAD_DIRECTORY}/{filename}')
            resp = make_response(
                file_backend.get_file_as_response(
                    filename=filename.split('/')[-1], storage=RESULT_DIRECTORY
                ), 201)
        else:
            resp = make_response(jsonify({'error': 'Wrong parameters'}), 404)
        return resp

    def _do_action(self, source):
        return NotImplementedError()


class FullCycle(Processing):
    def _do_action(self, source):
        logger.warning('sky replacing...')
        sky_replaced = bts.sky_replace(source=source)
        logger.warning('filtering...')
        return sky_replaced


class JustReplace(Processing):
    def _do_action(self, source):
        logger.warning('sky replacing...')
        return bts.sky_replace(source=source)


class JustFilter(Processing):
    def _do_action(self, source):
        logger.warning('filtering...')


api.add_resource(Root, '/')
api.add_resource(File, '/file')

api.add_resource(JustFilter, '/filter')
api.add_resource(JustReplace, '/replace')
api.add_resource(FullCycle, '/process')
