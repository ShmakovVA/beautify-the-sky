from PIL import Image
from cv2 import (
    imread,
    imwrite,
    cvtColor,
    threshold,
    resize,
    COLOR_BGR2GRAY,
    COLOR_GRAY2BGR,
    THRESH_OTSU,
    THRESH_BINARY_INV
)
from numpy import where
from torch import device, cuda, load, sigmoid
from torch.autograd import Variable
from torchvision import transforms

from rest_api.settings import (
    PTH_MODEL_PATH,
    CLEAR_SKY_PATH,
    UPLOAD_DIRECTORY,
    RESULT_DIRECTORY,
    SEGMENTS_DIRECTORY
)

DEVICE = device("cuda:0" if cuda.is_available() else "cpu")


class SkyBeautifier():
    def __init__(self, model_path=None):
        self.model_path = model_path or PTH_MODEL_PATH
        self.model = load(self.model_path, map_location=DEVICE)

    @staticmethod
    def implement_sky(img_path, sgm_path):
        output_path = sgm_path.replace(SEGMENTS_DIRECTORY, RESULT_DIRECTORY)
        # read images of source and segmentation
        img = imread(img_path)
        seg = imread(sgm_path)
        # read image of sky
        sky = imread(CLEAR_SKY_PATH)
        sky = resize(sky, dsize=(img.shape[1], img.shape[0]))
        # create mask for replacing by segmentation
        seg_gray = cvtColor(seg, code=COLOR_BGR2GRAY)
        _, mask = threshold(seg_gray, thresh=0, maxval=255,
                            type=THRESH_BINARY_INV | THRESH_OTSU)
        # convert mask to 3-channels
        mask = cvtColor(mask, code=COLOR_GRAY2BGR)
        # add sky to source (origin) image
        img[where(mask == 0)] = sky[where(mask == 0)]
        # save result
        imwrite(output_path, img)
        return output_path

    def magic(self, img_path):
        output_path = img_path.replace(UPLOAD_DIRECTORY, SEGMENTS_DIRECTORY)
        # read source (origin) file
        image = Image.open(img_path).convert('RGB')
        # torch transformation
        transform = transforms.Compose([
            transforms.Resize([224, 224]),
            transforms.ToTensor(),
            transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
        ])
        x = transform(image)
        # image and model to DEVICE
        img = Variable(x).to(DEVICE)
        self.model.to(DEVICE)
        # predict mask
        predictt = sigmoid(self.model(img.unsqueeze(0).to(DEVICE)))
        # get mask
        sky_mask = predictt.squeeze().cpu().detach().numpy().round()
        res = Image.fromarray(sky_mask.astype('uint8')).resize(image.size)
        # save result mask
        res.save(output_path)
        return output_path

    def sky_replace(self, source):
        return self.implement_sky(img_path=source, sgm_path=self.magic(source))
